
# [1.1.4](https://bitbucket.org/skylogistics/shopify-merchant/?at=v1.1.4) (2018-09-20)
### Features
* **`Fix`:** Eliminate unnecessary code that verifies if there are products, because previously all the products are eliminated in the function syncronize. ([b2e432a](https://bitbucket.org/skylogistics/shopify-merchant/commits/b2e432a41ce37eaecebd348879c4bc79274a6bb5))

* **`Update`:** Replace "removeProductCart" function to "deleteProductsCart" function in synchronize to eliminate more than one product at a time. ([5a8d26f](https://bitbucket.org/skylogistics/shopify-merchant/commits/5a8d26f9671329ac10952777f37e7ba037c7547c))

# [1.1.3](https://bitbucket.org/skylogistics/shopify-merchant/?at=v1.1.3) (2018-06-20)

### Features
* **`Update`:** Validate "SKBX_COUNTRY_HAS_CHANGED" in the synchronize function. If the value is 1 remove "auth-store" and reload the page, if the value is 2 delete "SKBX_COUNTRY_HAS_CHANGED" and continue the process ([7cc272e](https://bitbucket.org/skylogistics/shopify-merchant/commits/7cc272e5d137237829cd2b408e07b26b1c9040d2))

# [1.1.0](https://bitbucket.org/skylogistics/shopify-merchant/?at=v1.1.0) (2018-05-23)

### Features
* **`Fix`:** Minor Bugs  ([cdd8682](https://bitbucket.org/skylogistics/shopify-merchant/commits/cdd8682664392f3555a5a810f60b4f62fe8cba51))

# [1.0.10](https://bitbucket.org/skylogistics/shopify-merchant/?at=v1.1.0) (2018-04-24)

### Features
* **`Fix`:** Update multipleCalculate, searchVariant, synchronize cart  ([62b555a](https://bitbucket.org/skylogistics/shopify-merchant/commits/62b555a382f99540cae99bac04dde42501128b1a))

### Features
* **`Fix`:** Just call the getConcepts function when the international-checkout class exists  ([37ba8ef](https://bitbucket.org/skylogistics/shopify-merchant/commits/37ba8efa16cdf425a70c6d618842ee63d0062cbe))