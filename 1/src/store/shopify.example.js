import { setTimeout } from 'timers';
import SkyboxSDK from '@skyboxcheckout/merchant-sdk';
// Storage
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]      
};
const sessionStore = engine.createStore(storage.session, []);
const localStore = engine.createStore(storage.local, []);
const shopifyStyles = require('../../assets/css/shopify.css');
const _ = require('underscore');
// Local libs
const __cnStore = require('../../config/store.json');
const api = require('../xhr');

window.Sdk = new SkyboxSDK({
  IDSTORE: __cnStore.IDSTORE,
  MERCHANT: __cnStore.MERCHANT,
  MERCHANTCODE: __cnStore.MERCHANTCODE,
  STORE_URL: __cnStore.STORE_URL,
  SUCCESSFUL_PAGE: __cnStore.SUCCESSFUL_PAGE,
  CHECKOUT_PAGE: __cnStore.CHECKOUT_PAGE,
  CHECKOUT_BUTTON_CLASS: __cnStore.CHECKOUT_BUTTON_CLASS,
  CHANGE_COUNTRY_TEXT_USA: __cnStore.CHANGE_COUNTRY_TEXT_USA,
  SKBX_LOADER_NAME: __cnStore.SKBX_LOADER_NAME
});

const rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/" + __cnStore.SKBX_LOADER_NAME;

import { multipleCalculate } from './core/multipleCalculate.js';
import { synchronizeCart } from './core/synchronizeCart.js';
import { searchVariant } from './core/searchVariant.js';

$(document).ready(function () {  
  window.stateXhr = 0;
  // render change country
  Sdk.Common().initChangeCountry(); 

  // Get Event Listener
  (function (open) {
    XMLHttpRequest.prototype.open = function () {
      this.addEventListener("readystatechange", function(e) {
        var respondURL = Sdk.Common().detectInternetExplorer() ? this._url : this.responseURL;

        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { 
          return p.toString() === "[object SafariRemoteNotification]"; 
        })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
  
        if (isSafari) {
          respondURL = this._url;
        }

        if (!_.isUndefined(respondURL)) {           
          if (this.status == 200 && this.readyState == 4) { 
            // to synchronize store products with skybox checkout
            if (respondURL.match(/[/]cart.js[?]_=/gi)) {
              if (this.responseText && this.responseText.length > 0) {
                window.stateXhr++;
                synchronizeCart(this);
              }
            }
            // listen to additional requests 
          }
        }
      }, false);
      if (Sdk.Common().detectInternetExplorer())
        this._url = arguments[1];
      open.apply(this, arguments);
    };
  })(XMLHttpRequest.prototype.open);

  //listen variants
  $('body').on('change', 'select', function (e) {
    var currentUrl = $(location).attr('href');

    if (currentUrl.match(/[?]variant=/gi)) {
      var currentCode = currentUrl.split("?variant=")[1];
      searchVariant(currentCode, false, null, __cnStore.SKBX_LOADER_NAME);
    }
  });

  // Load less in checkout page and success page
  if (window.location.href.indexOf(__cnStore.CHECKOUT_PAGE) === -1 && 
      window.location.href.indexOf(__cnStore.SUCCESSFUL_PAGE) === -1) {
    setTimeout(function() {
      $.ajax({
        url: '/cart.js',
        method: 'GET',
        dataType: 'text',
        cache: false
      });

      // Load multipleCalculate less in /cart
      if (window.location.href.indexOf('/cart') === -1) {
        var currentUrl = $(location).attr('href');

        if (currentUrl.match(/[?]variant=/gi)) {
          var currentCode = currentUrl.split("?variant=")[1];
          searchVariant(currentCode, false, null, __cnStore.SKBX_LOADER_NAME);
        } else {
          multipleCalculate();
        }
      }
    }, 500);
  }

  // Skybox Ckeckout page
  if ($(location).attr('href').indexOf(__cnStore.CHECKOUT_PAGE) > -1) {
    $('#skybox-checkout-change-country').hide();
    $('#skybox-international-checkout').hide();
    // here code to hide mini cart icon

    Sdk.showCheckoutPageV2();
  }

  // Skybox invoice & Success Page
  if ($(location).attr('href').indexOf(__cnStore.SUCCESSFUL_PAGE) > -1) {
    $('#skybox-checkout-change-country').hide();
    $('#skybox-international-checkout').hide();
    // here code to hide mini cart icon

    if ($('#skybox-international-checkout-invoice').length > 0) {
      $('#skybox-international-checkout-invoice').html(`
        <h1 id="mensaje" style="text-align:center;">
          <img src="${rutaLoaderGif}"/>
        </h1>
      `);
      
      api.get(__cnStore.STORE_URL + '/cart/clear.js?_=' + $.now()).then(function() {
        Sdk.getCartInvoice().then(function (content) {
          var contentHTML = JSON.parse(content).Data.Invoice;
          var invoice = document.getElementById('skybox-international-checkout-invoice');

          invoice.innerHTML = contentHTML;
          sessionStore.remove('cart_prod_arr');
        });
      });
    }
  }
});