import { multipleCalculate } from './multipleCalculate.js';

export function searchVariant(currentCode, isModal, elemBtnAdd, loaderName) {
  currentCode = currentCode.split("%")[0];

  var classInternationalPrice = ".internationalPrice";

  if (isModal) {
    classInternationalPrice = ".js-quick-shop .internationalPrice";
  }
  
  $(classInternationalPrice).each(function(i, element) {
    var product = JSON.parse(element.getAttribute('data'));
    var rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderGris.gif";
          
    if (loaderName) {
      rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/" + loaderName;
    }

    product.variants.forEach(function (el, a) {
      var variantCode = el.id;
      if (variantCode == currentCode) {
        if (!isModal) {
          var padre = $(element).parent();
          padre.find("div:first-child").addClass('skbx-loader-' + currentCode);
        } else {
          var imgLoad = '<img style="height: 30px;" src="'+ rutaLoaderGif + '">';
          $(element).html(imgLoad);
        }
        element.setAttribute('id','skybox-product-price-' + currentCode);
        element.setAttribute('data-variant-id', currentCode);
        product.variants.splice(a, 1);
        product.variants.unshift(el);
        product = JSON.stringify(product);
        element.setAttribute('data', product);

        multipleCalculate([element], elemBtnAdd);
        return false;
      }
    });
  })      
}