const _ = require('underscore');

export function validateRestriction(arraySky, arrayStore, currentCountry) {
  var clonedArrStore = arrayStore,
      objUpdate = { updates: {}},
      html = '';

  var hasContent = false;

  if (_.isArray(arraySky) && arraySky.length > 0) {        
    html += '<div>';
    html += '<br />';
    html += ' <p class="restricted-msg">' + 'The following products will be removed from your shopping cart, they are' + '<strong>' + ' restricted in ' + currentCountry + '</strong>';
    html += ' <table>';
    html += '   <tbody>';

    arraySky.map(function(item) {
      for (var i in clonedArrStore) {
        var objStore = clonedArrStore[i],
            code = objStore.variant_id ? String(objStore.variant_id) : String(objStore.id);

        if ((item.HtmlObjectId).trim() === (code).trim()) {
          hasContent = true;
          objUpdate.updates[objStore.variant_id] = 0;

          html += '     <tr>';
          html += '       <td>';
          html += '         <center><img style="height: 150px;" src="' + objStore.image + '"</img>' + '</center>';
          html += '       </td>';
          html += '       <td>';
          html += '         <strong> <p class="restricted-msg">' + objStore.title + '</p>' + '</strong>' + '<br />';
          html += '       </td>';
          html += '     </tr>';
          break;
        }
      }
    });

    html += '   </tbody>';
    html += ' </table>';
    html += '</div>';
  }

  return {
    objUpdate: objUpdate,
    html: hasContent ? html : ''
  }
}