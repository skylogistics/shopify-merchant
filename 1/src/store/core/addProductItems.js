const _ = require('underscore');
// Local
const __cnStore = require('../../../config/store.json');

export function addProductItems(products) {
  var ListProducts = [];

  products.forEach(function (productItem) {
    var title = productItem.product_title;
    var getWeight = Sdk.calcWeightAndUnit(productItem.grams);

    if (__cnStore.CONF.PRODUCT_TYPE_DEFAULT !== '') {
      productItem.product_type = __cnStore.CONF.PRODUCT_TYPE_DEFAULT;
    }

    var _Optionals = {
      CustomFields : [
        String(productItem.sku)
      ]
    }

    var variant_title = (_.isNull(productItem.variant_title))? '': '-'+productItem.variant_title;

    var Product = {
      HtmlObjectid: String(productItem.variant_id),
      Id: 0,
      Sku: String(productItem.variant_id),
      Name: title + variant_title,
      Category: productItem.product_type,
      Price: (productItem.price / 100),
      ImgUrl: productItem.image,
      Language: "",
      Weight: getWeight.weight,
      WeightUnit: getWeight.unit,
      VolumetricWeight: 0,
      DefinitionOpt: "",
      Quantity: productItem.quantity,
      variantMerchantId: String(productItem.variant_id),
      productMerchantId: String(productItem.product_id)

    };

    ListProducts.push(
      { "Product": Product,
        "Optionals": _Optionals
      }
    );
  });

  return { "ListProducts": ListProducts };
}