const __cnStore = require('../../../config/store.json');

var rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderGris.gif";
          
if (__cnStore.SKBX_LOADER_NAME) {
  rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/" + __cnStore.SKBX_LOADER_NAME;
}

export function showLoaders() {
  $("[class*=skbx-loader]").html(`
    <center>
      <div><img style="height: 15px;width: 100px !important" src="${rutaLoaderGif}" /></div>
    </center>
  `);
}