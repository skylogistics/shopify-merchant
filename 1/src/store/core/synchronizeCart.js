const _ = require('underscore');
const arrayDiff = require('simple-array-diff');
const tingle = require('tingle.js');
// Storage
const engine = require('store/src/store-engine');
const storage = {
  local: [require('store/storages/localStorage')],
  session: [require('store/storages/sessionStorage')]      
};
const sessionStore = engine.createStore(storage.session, []);
// Local
const api = require('../../xhr');
const __cnStore = require('../../../config/store.json');

import { showLoaders } from "./showLoaders.js";
import { updateShoppingCart_hrml } from './updateShoppingCart_hrml.js';
import { addProductItems } from './addProductItems.js';
import { validateRestriction } from './validateRestriction.js';

export function synchronizeCart(data) {
  if (window.stateXhr == 1) {
    window.stateXhr++;

    var rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/loaderGris.gif";
          
    if (__cnStore.SKBX_LOADER_NAME) {
      rutaLoaderGif = "https://s3.amazonaws.com/sky-sbc-images/WebApp/SBC/Images/loaders/" + __cnStore.SKBX_LOADER_NAME;
    }

    if (data.responseText && data.responseText.length > 0) {
      var itemsStore = JSON.parse((data.responseText)).items,
          arrayRestricted = [],
          operations = [];

          Sdk.getLocationAllow().then(function () {
            var Cart = sessionStore.get('cart_prod_arr');
            var itemsSky = (!_.isUndefined(Cart) && !_.isNull(Cart) && Cart !== "[]") ? Cart.Data.Cart.Items : [];
    
            if (itemsStore.length > 0) {
              var lastCountry = sessionStore.get('last_country').toUpperCase().trim();
              var currentCountry = (sessionStore.get('auth-store').Data.CART_SKY.Country.Name).toUpperCase().trim();
    
              if (currentCountry !== lastCountry) {
                var loaderModal = new tingle.modal({
                  footer: false,
                  stickyFooter: false,
                  closeMethods: []
                });
    
                loaderModal.setContent(
                  '<center>' +
                  '<span class="restricted-msg"> Wait a moment please </span>' +
                  '<div style="padding-top: 10px;">' +
                  '<img src="' + rutaLoaderGif + '"/>' +
                  '</div>' +
                  '</center>'
                );
    
                loaderModal.open();
    
                Sdk.getCartRefresh().then(function (Cart) {
                  itemsSky = (!_.isUndefined(Cart) && !_.isNull(Cart)) ? Cart.Data.Cart.Items : [];
    
                  var restrictedModal = new tingle.modal({
                    footer: true,
                    stickyFooter: true,
                    closeMethods: ['overlay', 'button', 'escape'],
                    onClose: function () {
                      setTimeout(function () {
                        var current = top.location.href;
                        top.location.href = current;
                      }, 100);
                    }
                  });
    
                  if (_.isArray(itemsSky) && itemsSky.length > 0) {
                    arrayRestricted = _.filter(itemsSky, function (prod) {
                      var restricted = prod.IsRestricted;
                      return restricted === true;
                    });
    
                    for (var i = 0; i < itemsStore.length; i++) {
                      itemsStore[i].Code = String(itemsStore[i].variant_id);
                      itemsStore[i].Quantity = String(itemsStore[i].quantity);
                    }
    
                    var result = arrayDiff(itemsSky, itemsStore, 'Code');
    
                    var cart_prod_arr_add = result.added,
                      cart_prod_arr_remove = result.removed,
                      cart_prod_arr_common = result.common;
    
                    if (cart_prod_arr_add.length > 0) {
                      var prod = addProductItems(cart_prod_arr_add);
                      operations.push(Sdk.addProductsCart(prod));
                    }

                    var array_diff = [];
    
                    if (cart_prod_arr_common.length > 0) {
                      for (var a = 0; a < cart_prod_arr_common.length; a++) {
                        var itemStore = cart_prod_arr_common[a];
                        for (var b = 0; b < itemsSky.length; b++) {
                          var itemSky = itemsSky[b];
    
                          if (itemStore.Code === itemSky.Code) {
                            if ((itemStore.Quantity).toString() !== itemSky.Quantity.toString()) {
                              array_diff.push({
                                Id: itemSky.Id,
                                Quantity: itemStore.Quantity
                              });
                            }
                          }
                        }
                      }
    
                      if (array_diff.length > 0) {
                        var Products = { 'Product': array_diff };
                        operations.push(Sdk.editProductsCart(Products));
                      }
                    }

                    var arrItemsRemove = [];
    
                    if (cart_prod_arr_remove.length > 0) {
                      for (var a = 0; a < cart_prod_arr_remove.length; a++) {
                        var id = cart_prod_arr_remove[a].Id;
                        if (!_.isNull(id) && !_.isUndefined(id) && id !== "") {
                          arrItemsRemove.push({ Id: id});
                        }
                      }
                    }
    
                    if (arrayRestricted.length > 0) {
                      for (var a = 0; a < arrayRestricted.length; a++) {
                        var id = arrayRestricted[a].Id;
                        if (!_.isNull(id) && !_.isUndefined(id) && id !== "") {
                          arrItemsRemove.push({ Id: id});
                        }
                      }
                    }

                    if (arrItemsRemove.length > 0) {
                      arrItemsRemove = _.uniq(arrItemsRemove, false, function (item) {
                        return item.Id;
                      });
                      operations.push(Sdk.deleteProductsCart({ "Product": arrItemsRemove }));
                    }
    
                    Promise.all(operations).then(function (response) {
                      showLoaders();
                      var productsResponse = (response[0] && response[0].Data) ? response[0].Data.ListProducts : [];
    
                      arrayRestricted.map(function(item) {
                        productsResponse.push({
                          Success: false,
                          HtmlObjectId: String(item.Code),
                          Id: item.Id
                        })
                      });
    
                      var arrayRemove = _.filter(productsResponse, function (prod) {
                        var success = prod.Success;
                        return success === false;
                      });
    
                      if (arrayRemove.length < 1) {
                        Sdk.getCartRefresh().then(function (cart) {
                          sessionStore.set('cart_prod_arr', cart);
                          Sdk.Common().changeLastCountry(
                            sessionStore.get('auth-store').Data.CART_SKY.Country.Name
                          );
                          updateShoppingCart_hrml(cart);
                          $('.skbx-loader-subtotal').hide();
                          Sdk.Common().initBtnSkyCheckout();
                          loaderModal.close();
                          var ick = $('.international-checkout');
                          if (ick.length > 0) {
                            Sdk.getConcepts().then(function (html) {
                              var htmlCart = Sdk.Common().conceptsTableCart(html);
                              ick.html(htmlCart);
                              window.stateXhr = 0;
                            });
                          } else {
                            window.stateXhr = 0;
                          }
                        });
                      }
                      else {
                        var validate = validateRestriction(arrayRemove, itemsStore, currentCountry),
                          update = validate.objUpdate,
                          html = validate.html;
    
                        if (Object.keys(update.updates).length > 0) {
                          api.post('/cart/update.js', update).then(function (respuesta) {
                            if (html.length > 0) {
                              restrictedModal.setContent(html);
                              restrictedModal.addFooterBtn('Close', 'tingle-btn tingle-btn--default tingle-btn--pull-right', function () {
                                restrictedModal.close();
                              });
                            }
    
                            Sdk.getCartRefresh().then(function (cart) {
                              sessionStore.set('cart_prod_arr', cart);
                              Sdk.Common().changeLastCountry(
                                sessionStore.get('auth-store').Data.CART_SKY.Country.Name
                              );
                              updateShoppingCart_hrml(cart);
                              $('.skbx-loader-subtotal').hide();
                              Sdk.Common().initBtnSkyCheckout();
                              loaderModal.close();
                              restrictedModal.open();
                              var ick = $('.international-checkout');
                              if (ick.length > 0) {
                                Sdk.getConcepts().then(function (html) {
                                  var htmlCart = Sdk.Common().conceptsTableCart(html);
                                  ick.html(htmlCart);
                                  window.stateXhr = 0;
                                });
                              } else {
                                window.stateXhr = 0;
                              }
                            });
                          });
                        } else {
                          window.stateXhr = 0; 
                        }
                      }
                    });
                  }
                  else {
                    var prod = addProductItems(itemsStore);
    
                    Sdk.addProductsCart(prod).then(function (response) {
                      Sdk.Common().elementVisibleStore(false);
                      var productsResponse = response.Data ? response.Data.ListProducts : [];
                      var arrayRemove = _.filter(productsResponse, function (prod) {
                        var success = prod.Success;
                        return success === false;
                      });
    
                      if (arrayRemove.length < 1) {
                        Sdk.getCartRefresh().then(function (cart) {
                          sessionStore.set('cart_prod_arr', cart);
                          Sdk.Common().changeLastCountry(
                            sessionStore.get('auth-store').Data.CART_SKY.Country.Name
                          );
                          updateShoppingCart_hrml(cart);
                          Sdk.Common().initBtnSkyCheckout();
                          loaderModal.close();
                          $('.skbx-loader-subtotal').hide();
                          var ick = $('.international-checkout');
                          if (ick.length > 0) {
                            Sdk.getConcepts().then(function (html) {
                              var htmlCart = Sdk.Common().conceptsTableCart(html);
                              ick.html(htmlCart);
                              window.stateXhr = 0;
                            });
                          } else {
                            window.stateXhr = 0;
                          }
                        });
                      }
                      else {
                        var validate = validateRestriction(arrayRemove, itemsStore, currentCountry),
                          update = validate.objUpdate,
                          html = validate.html;
    
                        if (Object.keys(update.updates).length > 0) {
                          api.post('/cart/update.js', update).then(function () {
                            if (html.length > 0) {
                              restrictedModal.setContent(html);
                              restrictedModal.addFooterBtn('Close', 'tingle-btn tingle-btn--default tingle-btn--pull-right', function () {
                                restrictedModal.close();
                              });
                            }
    
                            Sdk.getCartRefresh().then(function (cart) {
                              sessionStore.set('cart_prod_arr', cart);
                              Sdk.Common().changeLastCountry(
                                sessionStore.get('auth-store').Data.CART_SKY.Country.Name
                              );
                              updateShoppingCart_hrml(cart);
                              $('.skbx-loader-subtotal').hide();
                              Sdk.Common().initBtnSkyCheckout();
                              loaderModal.close();
                              restrictedModal.open();
                              var ick = $('.international-checkout');
                              if (ick.length > 0) {
                                Sdk.getConcepts().then(function (html) {
                                  var htmlCart = Sdk.Common().conceptsTableCart(html);
                                  ick.html(htmlCart);
                                  window.stateXhr = 0;
                                });
                              } else {
                                window.stateXhr = 0;
                              }
                            });
                          });
                        } else {
                          window.stateXhr = 0;
                        }
                      }
                    });
                  }
                });
              } else {
                if (_.isArray(itemsSky) && itemsSky.length > 0) {
                  for (var i = 0; i < itemsStore.length; i++) {
                    itemsStore[i].Code = String(itemsStore[i].variant_id);
                    itemsStore[i].Quantity = String(itemsStore[i].quantity);
                  }
    
                  var result = arrayDiff(itemsSky, itemsStore, 'Code');
    
                  var cart_prod_arr_add = result.added,
                    cart_prod_arr_remove = result.removed,
                    cart_prod_arr_common = result.common;
    
                  if (cart_prod_arr_add.length > 0) {
                    var prod = addProductItems(cart_prod_arr_add);
                    operations.push(Sdk.addProductsCart(prod));
                  }

                  if (cart_prod_arr_common.length > 0) {
                    var array_diff = [];
    
                    for (var a = 0; a < cart_prod_arr_common.length; a++) {
                      var itemStore = cart_prod_arr_common[a];
                      for (var b = 0; b < itemsSky.length; b++) {
                        var itemSky = itemsSky[b];
    
                        if (itemStore.Code === itemSky.Code) {
                          if ((itemStore.Quantity).toString() !== itemSky.Quantity.toString()) {
                            array_diff.push({
                              Id: itemSky.Id,
                              Quantity: itemStore.Quantity
                            });
                          }
                        }
                      }
                    }
    
                    if (array_diff.length > 0) {
                      var Products = { 'Product': array_diff };
                      operations.push(Sdk.editProductsCart(Products));
                    }
                  }

                  var arrItemsRemove = [];
    
                  if (cart_prod_arr_remove.length > 0) {
                    for (var a = 0; a < cart_prod_arr_remove.length; a++) {
                      var id = cart_prod_arr_remove[a].Id;
                      if (!_.isNull(id) && !_.isUndefined(id) && id !== "") {
                        arrItemsRemove.push({ Id: id});
                      }
                    }
                  }

                  if (arrItemsRemove.length > 0) {
                    arrItemsRemove = _.uniq(arrItemsRemove, false, function (item) {
                      return item.Id;
                    });
              
                    operations.push(Sdk.deleteProductsCart({ "Product": arrItemsRemove }));
                  }
    
                  Promise.all(operations).then(function () {
                    showLoaders();
                    Sdk.getCartRefresh().then(function (Cart) {
                      
                      sessionStore.set('cart_prod_arr', Cart);
                      updateShoppingCart_hrml(Cart);
                      Sdk.Common().initBtnSkyCheckout();
                      window.stateXhr = 0;
                      $('.skbx-loader-subtotal').hide();
                      var ick = $('.international-checkout');
                      if (ick.length > 0) {
                        Sdk.getConcepts().then(function (html) {
                          var htmlCart = Sdk.Common().conceptsTableCart(html);
                          ick.html(htmlCart);
                        });
                      }
                    });
                  }).catch(function (error) {
                    console.log(':: synchronizeCart --> Promise.all(operations) error', error);
                  });
                }
                else {
                  var prod = addProductItems(itemsStore);
                  Sdk.addProductsCart(prod).then(function () {
                    Sdk.getCartRefresh().then(function (Cart) {
                      
                      sessionStore.set('cart_prod_arr', Cart);
                      updateShoppingCart_hrml(Cart);
                      $('.skbx-loader-subtotal').hide();
                      Sdk.Common().initBtnSkyCheckout();
                      window.stateXhr = 0;
                      var ick = $('.international-checkout');
                      if (ick.length > 0) {
                        Sdk.getConcepts().then(function (html) {
                          var htmlCart = Sdk.Common().conceptsTableCart(html);
                          ick.html(htmlCart);
                        });
                      }
                    });
                  });
                }
              }
            }
            else {
              if (itemsSky.length) {
                Sdk.deleteProductsCart().then(function () {
                  sessionStore.remove('cart_prod_arr');
                  window.stateXhr = 0;
                });
              } else {
                sessionStore.remove('cart_prod_arr');
                window.stateXhr = 0;
              }
            }
          }).catch(function (error) {
            console.log(':: getLocationAllow error ', error);
          });
    }
  }
}