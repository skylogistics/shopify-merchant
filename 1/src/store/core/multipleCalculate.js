import { showLoaders } from "./showLoaders";
const _ = require('underscore');
// Local
const api = require('../../xhr');
const __cnStore = require('../../../config/store.json');

function showPriceMultiC(productId, dataResp) {
  var prefModal = ($(".remodal-is-opened").length > 0) ? ".js-quick-shop " : "";// only for modal
  $(prefModal + '#skybox-product-price-' + productId)
    .html(dataResp.responseText)
    .removeClass('empty-price');
  $(".skbx-loader-" + productId).hide();  
}

export function multipleCalculate (variant, elemBtnAdd) {
  if (_.isUndefined(elemBtnAdd) || _.isNull(elemBtnAdd) || elemBtnAdd == '') {
    elemBtnAdd = '.Sky--btn-add';
  }

  Sdk.getLocationAllow().then(function () {
    $(elemBtnAdd).addClass('disabled');
    $(elemBtnAdd).attr('disabled', true);
    showLoaders();

    Sdk.getAuthCart().then(function (store) {
      var ListProducts_Array = [],
          ListProducts = {};

      $(".internationalPrice").each(function(i, element) {
        var elementId = element.getAttribute('id'),
            product = JSON.parse(element.getAttribute('data'));

        var variantID = element.getAttribute('data-variant-id');
        var productID = element.getAttribute('data-product-id');

        if (elementId && product) {
          var id_shopify_product_variant = String(variantID),
              title = product.variants[0].name,
              price = parseFloat(product.variants[0].price / 100),
              images = product.featured_image,
              sku = variantID,
              product_type = product.type;

          var getWeight = Sdk.calcWeightAndUnit(product.variants[0].weight);

          if (!id_shopify_product_variant) {
            id_shopify_product_variant = String(product.id);
          }

          if (__cnStore.CONF.PRODUCT_TYPE_DEFAULT !== '') {
            product_type = __cnStore.CONF.PRODUCT_TYPE_DEFAULT;
          }

          var idObj = id_shopify_product_variant + '__' + Sdk.Common().getDate() + store.Data.CART_SKY.Country.Iso + store.Data.CART_SKY.Cart.Currency;
          
          var _Optionals = {
            CustomFields : [
              product.variants[0].sku
            ]
          };

          if (product_type !== '') {
            ListProducts_Array.push(
              {
                HtmlObjectid: idObj,
                Sku: sku,
                Name: title,
                Category: product_type,
                Price: price,
                ImgUrl: images,
                Weight: getWeight.weight,
                WeightUnit: getWeight.unit,
                variantMerchantId: id_shopify_product_variant,
                productMerchantId: String(productID),
                Optionals : _Optionals
              }
            );
          }
          else {
            console.log('::weight', id_shopify_product_variant + ',' + product_type);
            $(".skbx-loader-" + id_shopify_product_variant).html('<span>Product not available in your country</span>');
            $('form[data-product-id="'+ productID +'"] ' + elemBtnAdd).hide();
          }
        }
      });

      ListProducts_Array = _.uniq(ListProducts_Array, false, function (pred) {
        return pred.HtmlObjectid;
      });

      ListProducts = { ListProducts: ListProducts_Array };

      if (ListProducts_Array.length > 0) {
        Sdk.getMulticalculate(ListProducts).then(function(urlHtml) {
          // add new attribute: success [state]
          urlHtml.Data.forEach(function(element, index) {
            element.success = false;
            element.productMercId = ListProducts.ListProducts[index].productMerchantId;
          });

          var count = 0;
          var showPriceTimer = setInterval(function () {
            getHtmlMultipleCalculate(urlHtml);
            // Verify all success states from every urlHTML object
            urlHtml.Data.forEach(function(element) {
              if (element.success) {
                count+=1;
              }
            });
            // Kill setInterval
            if (count == urlHtml.Data.length) {
              clearInterval(showPriceTimer);
            }
          }, 1000);
        }).catch(function (error) {
          console.log(error);
          $("span[class^='skbx-price']").hide();
        });
      }

      function getHtmlMultipleCalculate(res) {
        var productId = "";
        var url = "";
        var __Res = [];

        console.info("::Res Multi", res);

        for (var i = 0; i < res.Data.length; i++) {
          productId = res.Data[i].HtmlObjectId.toString().split('__')[0];
          url = res.Data[i].Url;
          (function (productId, url, i) {
            //Verify if the success status is false
            var x = productId;
            if (!res.Data[i].success) {
              return api.get(url).then(function (dataResp) {
                //restricted product
                if (dataResp.responseText.length > 1) {
                  showPriceMultiC(productId, dataResp);
                  if ($(dataResp.responseText).is('span.not-available')) {
                    $('form[data-product-id="'+ res.Data[i].productMercId +'"] ' + elemBtnAdd).hide();
                  } else {
                    $('form[data-product-id="'+ res.Data[i].productMercId +'"] ' + elemBtnAdd).show();
                    $('form[data-product-id="'+ res.Data[i].productMercId +'"] ' + elemBtnAdd).attr('disabled', false);
                    $('form[data-product-id="'+ res.Data[i].productMercId +'"] ' + elemBtnAdd).removeClass('disabled');
                  }

                  $('#' + 'skybox-product-price-' + productId).html(dataResp.responseText);
                  $('.skybox-product-price-' + productId).html(dataResp.responseText);
                  $(".skbx-loader-" + productId).hide();
                  // Change the success status to avoid another request
                  res.Data[i].success = true;           
                }
              }).catch(function (error) {
                console.log('getHtmlMultipleCalculate', error);
              });
            }
          })(productId, url, i);
        }
      }
    });
  }).catch(function (error) {
    console.warn('LocationAllow::', error);
    $(elemBtnAdd).show();
    $(elemBtnAdd).attr('disabled', false);
    $(elemBtnAdd).removeClass('disabled');
  });
}